{ config, lib, ... }:
let
  inherit (lib) mkOption;
  inherit (lib.types) str;
in {
  options.domain = mkOption {
    type = str;
  };
  exports.letsencrypt.certs.${config.domain}= "/dummy/path";
  run = "echo dummy";
}
