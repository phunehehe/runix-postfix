let
  domain = "example.com";
  mail-domain = "mail.example.com";
in {
  archiveopteryx = {
    domain = mail-domain;
    db-password = "secret";
  };
  le-dummy = {
    domain = mail-domain;
  };
  opendkim = {
    inherit domain;
  };
  postfix = {
    inherit domain;
    hostname = mail-domain;
    ssl-domain = mail-domain;
  };
}
