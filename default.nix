{ config, exports, home, lib, pkgs, ... }:
let

  inherit (lib) catAttrs concatMapStrings head mapAttrsToList optionalString;

  ln = "${pkgs.coreutils}/bin/ln --force --symbolic";

  dataDir = "${home}/data";
  spoolDir = "${home}/mail-spool";
  queueDir = "${home}/queue";
  saslauthdRunDir = "${home}/saslauthd/run";

  # `newaliases` wants the `postfix` user (mail_owner) to exists so this has to
  # be built at run time.
  config-directory = "${home}/config";

  postfix = pkgs.postfix.override {
    withSQLite = true;
  };

  le-dir = head (catAttrs config.ssl-domain (mapAttrsToList (_: value:
    value.letsencrypt.certs or {}
  ) exports));

  main-config = pkgs.writeText "main.cf" ''

    meta_directory       = ${config-directory}
    data_directory       = ${dataDir}
    mail_spool_directory = ${spoolDir}
    queue_directory      = ${queueDir}

    smtpd_helo_restrictions      = warn_if_reject reject_non_fqdn_hostname
                                 , reject_invalid_hostname
                                 , permit
    smtpd_recipient_restrictions = permit_sasl_authenticated
                                 , reject_non_fqdn_recipient
                                 , reject_unauth_destination
                                 , reject_unauth_pipelining
                                 , reject_unknown_recipient_domain
                                 , permit
    smtpd_data_restrictions      = reject_unauth_pipelining
    smtpd_sender_restrictions    = permit_sasl_authenticated
                                 , warn_if_reject reject_non_fqdn_sender
                                 , reject_unauth_pipelining
                                 , reject_unknown_sender_domain
                                 , permit
    smtpd_relay_restrictions     = permit_mynetworks
                                 , permit_sasl_authenticated
                                 , reject_unauth_destination

    virtual_alias_maps      = proxy:sqlite:${config-directory}/${virtual-alias-map.name}
    virtual_mailbox_domains = proxy:sqlite:${config-directory}/${virtual-mailbox-domains.name}

    cyrus_sasl_config_path = ${config-directory}
    smtpd_sasl_path        = ${smtpd-conf-base}
    smtpd_sasl_type        = cyrus

    virtual_transport = lmtp:inet:127.0.0.1:${toString exports.archiveopteryx.lmtp-port}
    mailbox_transport = lmtp:inet:127.0.0.1:${toString exports.archiveopteryx.lmtp-port}

    smtpd_tls_cert_file      = ${le-dir}/fullchain.pem
    smtpd_tls_key_file       = ${le-dir}/key.pem
    smtpd_tls_security_level = may

    smtpd_milters = inet:127.0.0.1:${toString exports.opendkim.port}
    non_smtpd_milters = inet:127.0.0.1:${toString exports.opendkim.port}

    ${optionalString (config.hostname != null) "myhostname = ${config.hostname}"}

    compatibility_level = 2
    disable_vrfy_command = yes
    local_destination_recipient_limit = $default_destination_recipient_limit
    mydomain = ${config.domain}
    recipient_delimiter = +
    smtp_tls_CAfile = ${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt
    smtp_tls_security_level = may
    smtpd_helo_required = yes
    smtpd_sasl_auth_enable = yes
  '';

  # FIXME: this file is put there manually
  virtual-db = "${home}/virtual.sqlite";

  virtual-alias-map = pkgs.writeText "virtual-alias-map" ''
    dbpath = ${virtual-db}
    query = SELECT destination FROM aliases WHERE source='%s'
  '';

  virtual-mailbox-domains = pkgs.writeText "virtual-mailbox-domains" ''
    dbpath = ${virtual-db}
    query = SELECT 1 FROM users WHERE email LIKE '%%@%s'
  '';

  smtpd-conf-base = "smtpd";
  smtpd-conf = pkgs.writeText "${smtpd-conf-base}.conf" ''
    pwcheck_method: saslauthd
    mech_list: PLAIN LOGIN
  '';

  postmap = m:
  let link = "${config-directory}/${m.name}";
  in ''
    ${ln} ${m} ${link}
    ${postfix}/bin/postmap -c ${config-directory} ${link}
  '';
  postmaps = concatMapStrings postmap [
    virtual-alias-map
    virtual-mailbox-domains
  ];

  buildConfigDir = pkgs.writeBash "postfix-build-config-directory" ''

    ${ln} ${main-config} ${config-directory}/main.cf
    ${ln} ${postfix}/etc/postfix/master.cf ${config-directory}/
    ${ln} ${postfix}/etc/postfix/postfix-files ${config-directory}/
    ${ln} ${smtpd-conf} ${config-directory}/${smtpd-conf.name}

    setgid_group=$(${postfix}/bin/postconf -c ${config-directory} -h setgid_group)
    ${pkgs.glibc.bin}/bin/getent group "$setgid_group" || ${pkgs.shadow}/bin/groupadd --system "$setgid_group"

    ${ln} ${postfix}/etc/postfix/aliases ${config-directory}/
    #${postfix}/bin/newaliases -C ${config-directory}

    ${postmaps}
  '';

  le-renew-hook = ''
    ${postfix}/bin/postfix -c ${config-directory} reload
  '';

  dirs = [
    "${queueDir}/pid"
    "${queueDir}/private"
    "${queueDir}/public"
    config-directory
    dataDir
    saslauthdRunDir
    spoolDir
  ];

  user = "postfix";
  processes = {
    saslauthd = pkgs.writeBash "saslauthd" ''

      # Ideally there should be an option in saslauthd to use another dir
      ${ln} --no-target-directory ${saslauthdRunDir} /run/saslauthd

      exec ${pkgs.runit}/bin/chpst -u ${user} \
        ${pkgs.cyrus_sasl}/bin/saslauthd -a rimap -O localhost -dr
    '';

    postfix = pkgs.writeBash "postfix" ''

      ${pkgs.coreutils}/bin/mkdir --parents ${toString dirs}
      ${pkgs.coreutils}/bin/chown ${user} ${toString dirs}

      ${buildConfigDir}
      echo "Postfix logs to Syslog. Check that too."
      exec ${postfix}/libexec/postfix/master -c ${config-directory}
    '';
  };

in {
  exports.letsencrypt.renew-hooks.${config.ssl-domain} = le-renew-hook;
  options = {
    domain = lib.mkOption {
      type = lib.types.str;
    };
    ssl-domain = lib.mkOption {
      type = lib.types.str;
    };
    hostname = lib.mkOption {
      type = lib.types.nullOr lib.types.str;
      default = null;
    };
  };
  run = pkgs.runsvdir home processes;
}
